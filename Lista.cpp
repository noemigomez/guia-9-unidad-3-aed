#include <iostream>
using namespace std;

#include "hash.h"
#include "Lista.h"

Lista::Lista() {}

void Lista::agregar_numero(int numero) {
    Nodo *tmp;

    // crea un nodo
    tmp = new Nodo;
    // asigna número
    tmp->numero = numero;
    // null por defecto
    tmp->sig = NULL;

    //si es el primer nodo queda como raíz y último
    if (this->raiz == NULL) {
        this->raiz = tmp;
        this->ultimo = this->raiz;
    //sino apunta al último nodo y deja el nuevo como último
    }else{
      this->ultimo->sig = tmp;
      this->ultimo = tmp;
    }
}

void Lista::imprimir () {
  // para recorrer la lista
  Nodo *tmp = this->raiz;

  if(tmp==NULL){
    cout << "No hay colisiones" << endl;
  }

  // recorre hasta que llege al final, null
  cout << "Colisiones" << endl;
  while (tmp != NULL) {
      cout << "|" << tmp->numero << "|";
      tmp = tmp->sig;
  }
  cout << "\n";
}
