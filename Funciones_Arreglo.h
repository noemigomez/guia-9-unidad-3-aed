#include <iostream>
using namespace std;

#ifndef ARREGLO_H
#define ARREGLO_H

class Arreglo{
	public:
		// constructor
		Arreglo();

		// métodos
		int primo(int n);
    int funcion_hash(int numero, int n);
    void prueba_lineal(int n, int *arreglo, int numero, int posicion, bool busqueda);
    void prueba_cuadratica(int n, int *arreglo, int numero, int posicion, bool busqueda);
    void doble_direccion(int n, int *arreglo, int numero, int posicion, bool busqueda);
		void encadenamiento(int *arreglo, int n, int numero, int posicion, Lista *lista, bool busqueda);
		bool is_full(int n, int *arreglo);


	private:
		// no hay métodos privados
};
#endif
