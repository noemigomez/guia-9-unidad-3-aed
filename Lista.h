#include <iostream>
using namespace std;

#ifndef LISTA_H
#define LISTA_H

class Lista {
    private:
        Nodo *raiz = NULL;
        Nodo *ultimo = NULL;

    public:
        //constructor
        Lista();
        // agrega número y crea nuevo nodo
        void agregar_numero (int numero);
        //muestra la lista
        void imprimir ();
};
#endif
