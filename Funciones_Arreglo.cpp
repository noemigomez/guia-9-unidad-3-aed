#include <iostream>
using namespace std;

#include "hash.h"
#include "Lista.h"
#include "Funciones_Arreglo.h"

// Constructor
Arreglo::Arreglo(){}

// determina el primo antecesor
int Arreglo::primo(int n){
  // para eliminar
  n = n+1;
  int contador; // de divisores
  do{
    // contador 0 y se elimina un n para que funciona el ciclo
    contador = 0;
    n--;
    for(int i=n; i>=1; i--){
      // si no hay resto entonces es divisor
      if((n%i)==0){
        contador++;
      }
    }
  // contador mas que dos entonces no es primo y mientras aun queden n para descontar
  }while(contador>2 && n>0);
  return n;
}

// funcion hash por módulo
int Arreglo::funcion_hash(int numero, int n){
  // conviene que sea un número primo
  int divisor = primo(n);
  return (numero%divisor);
}

// solucion de colisiones prueba_lineal
void Arreglo::prueba_lineal(int n, int *arreglo, int numero, int posicion, bool busqueda){
  // si es que ya está
  if(arreglo[posicion]!=NULL && arreglo[posicion]==numero){
    cout << "Ese elemento ya se encuentra en la posicion" << posicion << endl;
  // si no está se agrega o busca por el metodo
  }else{
    // posicion_x temporal de posicion
    int posicion_x = posicion + 1;
    // mientras la posicion sea menor al largo, el arreglo en dicho lugar exista y no sea la misma posicion que un inicio
    while(posicion_x<=n && arreglo[posicion_x]!=NULL && arreglo[posicion]!=numero && posicion_x != posicion){
      // si es que se busca en lugar de agregar al arreglo
      if(busqueda){
        // si se encuentra se detiene el ciclo
        if(arreglo[posicion_x]==numero){
          break;
        }
      }
      // si no se encuentra o no se ha encontrado donde posicionarlo aumentamos la posicion para seguir buscando
      posicion_x++;
      // si llega al límite entonces se vuelve al inicio
      if(posicion_x==n){
        posicion_x = 0;
      }
    }
    // si no se busca, se agrega
    if(busqueda==false){
      arreglo[posicion_x] = numero;
    }
    // por si se encuentra o no
    if(arreglo[posicion_x]==NULL || posicion_x==posicion){
      cout << "La informacion no se encuentra en el arreglo" << endl;
    }else{
      cout << "La informacion esta en la posicion " << posicion_x << endl;
    }
    cout << "Ha ocurrido una colisión en la posición " << posicion << ",";
    cout << " número localizado en " << posicion_x << endl;
    // desplazamiento final del número
    int d_final = posicion_x - posicion;
    if(d_final<0){
      d_final = posicion - posicion_x;
    }
    cout << "Desplazamiento final: " << d_final << endl;
  }
}

// solucion de colisiones prueba cuadrática
void Arreglo::prueba_cuadratica(int n, int *arreglo, int numero, int posicion, bool busqueda){
  // si es que el numero ya está
  if(arreglo[posicion]!=NULL && arreglo[posicion]==numero){
    cout << "Ese elemento ya se encuentra en la posicion" << posicion << endl;
  // si no está se busca o se agrega
  }else{
    // para sumar cuadraticamente, comenzando por el 1
    int i = 1;
    // la lógica es si i es 1, posicion_x = posicion +1, si i es 2, posicion_x = posicion + 4
    int posicion_x = posicion + (i*i);
    // si el arreglo en dicho lugar no está vacío y no es el que se quiere colocar
    while(arreglo[posicion_x]!=NULL && arreglo[posicion_x]!=numero){
      // si se busca y se encuentra se detiene el ciclo
      if(busqueda){
        if(arreglo[posicion_x]==numero){
          break;
        }
      }
      // para seguir buscando para localizar o ver donde agregar el número
      i++;
      posicion_x = posicion + (i*i);
      // si se sobrepasa del largo vuelve al inicio
      if(posicion_x >= n){
        i = 0;
        posicion_x = 1;
        posicion = 1;
      }
    }
    // si no se busca se agrega
    if(busqueda==false){
      arreglo[posicion_x] = numero;
    }
    // imprime si se encontró y donde
    if(arreglo[posicion_x]==NULL){
      cout << "La informacion no se encuentra en el arreglo" << endl;
    }else{
      cout << "La informacion esta en la posicion " << posicion_x << endl;
    }
    cout << "Ha ocurrido una colisión en la posición " << posicion << ",";
    cout << " número localizado en " << posicion_x << endl;
    // pide desplazamiento final
    int d_final = posicion_x - posicion;
    if(d_final<0){
      d_final = posicion - posicion_x;
    }
    cout << "Desplazamiento final: " << d_final << endl;
  }
}

// solucion de colisiones doble dirección
void Arreglo::doble_direccion(int n, int *arreglo, int numero, int posicion, bool busqueda){
  // si ya se encuentra el número en el arreglo
  if(arreglo[posicion]!=NULL && arreglo[posicion]==numero){
    cout << "Ese elemento ya se encuentra en la posicion" << posicion << endl;
  // si no se encunetra se busca o se ve donde se coloca
  }else{
    // una nueva posicion dada por una función hash
    int posicion_x = funcion_hash(posicion+1, n);
    // mientras la posicion sea menor al largo, el arreglo ahí exista, sea distinto del número y no volvamos a la misma posicion
    while(posicion_x<=n && arreglo[posicion_x]!=NULL && arreglo[posicion_x]!=numero && posicion_x!=posicion){
      // si se busca y se encuentra termina el ciclo
      if(busqueda){
        if(arreglo[posicion_x]==numero){
          break;
        }
      }
      // para continuar buscando
      posicion_x = funcion_hash(posicion_x+1, n);
    }
    // si no se busca se agrega al arreglo
    if(busqueda==false){
      arreglo[posicion_x] = numero;
    }
    // imprime localización
    if(arreglo[posicion_x]==NULL || arreglo[posicion_x]!=numero){
      cout << "La informacion no se encuentra en el arreglo" << endl;
    }else{
      cout << "La informacion esta en la posicion " << posicion_x << endl;
    }
    cout << "Ha ocurrido una colisión en la posición " << posicion << ",";
    cout << " número localizado en " << posicion_x << endl;
    // pide desplazamiento final
    int d_final = posicion_x - posicion;
    if(d_final<0){
      d_final = posicion - posicion_x;
    }
    cout << "Desplazamiento final: " << d_final << endl;
  }
}

// solucion de colisiones por encadenamiento
void Arreglo::encadenamiento(int *arreglo, int n, int numero, int posicion, Lista *lista, bool busqueda){
  cout << "Ha ocurrido una colisión en la posición " << posicion << endl;
  // nodos para la lista enlazada
  Nodo *tmp = NULL;
  Nodo *arreglo_temp[n];

  // arreglo tipo nodo
  for(int i=0; i<n; i++){
      arreglo_temp[i] = new Nodo();
      arreglo_temp[i]->sig = NULL;
      arreglo_temp[i]->numero = NULL;
  }

  // se llena con el arreglo actual
  for(int i=0; i<n; i++){
    arreglo_temp[i]->numero = arreglo[i];
    arreglo_temp[i]->sig = NULL;
  }

  // si es que ya está en el arreglo
  if(arreglo[posicion]!=NULL && arreglo[posicion]==numero){
    cout << "La información está en la posición " << posicion << endl;
  // si no entonces se busca
  }else{
    // temporal para agregar a enlazada de colisiones
    tmp = arreglo_temp[posicion]->sig;
    // mientras no sea vacío y el número no sea el mismo se sigue buscando
    while(tmp!=NULL && tmp->numero != numero){
      tmp = tmp->sig;
    }
    // si no se usa la función para buscar sino para agregar entonces se agrega la colision a la lista enlazada
    if(busqueda==false){
      lista->agregar_numero(numero);
      cout << "Agregado a lista enlazada de colisiones" << endl;
    }
    // imprime localizacion y lista enlazada
    if(tmp==NULL){
      cout << "La info no está en la lista" << endl;
    }else{
      cout << "La info se encuentra en la lista" << endl;
    }
    lista->imprimir();
  }
}

bool Arreglo::is_full(int n, int *arreglo){
  int contador = 0;
  bool llena;
  for(int i=0; i<n; i++){
    if(arreglo[i]!=NULL){
      contador++;
    }
  }
  if(contador==n){
    llena = true;
  }else{
    llena = false;
  }
  return llena;
}
