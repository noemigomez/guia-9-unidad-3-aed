# Métodos de Búsqueda - Tablas Hash

El programa crea un arreglo mediante funciones de tabla hash, una función mucho más eficiente para almacenar y buscar datos dentro de un arreglo. Dada una clave determina una posición. También dos claves pueden tener una misma clave por lo que también trata soluciones de estas llamadas colisiones. Permite tanto el ingreso como su búsqueda con 4 tipos de solución de colisión: prueba lineal, prueba cuadrática, doble dirección y encadenamiento.

## Comenzando

La función hash determinada en el programa es aquella por módulo o división, depende del resto de una división. Se divide la clave por el número primo antecesor al largo del arreglo para encontrar su posición dentro del arreglo.

En cuanto a los métodos de solución de colisión tenemos 4:

1. Prueba lineal: si ya está ocupado el lugar entonces recorre de uno en uno hasta encontrar un puesto.

2. Prueba cuadrática: si ya está ocupado entonces desde dicha posición se suman números cuadráticamente consecutivos hasta encontrar un espacio dentro del arreglo (posicion + 1, posicion + 4, ...).

3. Doble dirección: si ya está ocupado buscamos nuevas posiciones usando nuevamente la función hash.

4. Encadenamiento: se utiliza una lista enlazada para las colisiones.



## Prerrequisitos

### Sistema Operativo

Para ejecutar el programa se necesita un sistema operativo Linux, de preferencia Ubuntu o Debian.

### Make

Se necesita instalar un make para compilar el programa de manera sencilla. Se instala mediante el siguiente comando:

`sudo apt install make`

**En caso de no usar make**

Se puede ejecutar el programa sin make (revisar **Ejecutando las pruebas**).

## Ejecutando las pruebas

### Iniciar programa "compilar"

**Con make**

En la terminal situada en la carpeta con nuestros archivos se debe ejecutar el comando:

`make`

**Sin make**

En la terminar situada en la carpeta con nuestros archivos se debe ejecutar el siguiente comando:

`g++ hash.cpp Funciones_Arreglo.cpp Lista.cpp -o hash`

### Luego de compilar

Para ejecutar el programa una vez compilado se debe ejecutar el siguiente comando con parámetro de entrada:

`./hash {L|C|D|E}`

L, C, D y E determinan el método para solucionar la colisión y debe agregar solamente **uno** de los cuatro. Si ingresa más o ninguno de los determinados el programa no iniciará. Debe ser:

`./hash L` ; `./hash C` ; `./hash D` ; `./hash E` tanto mayúsculas como minúsculas

Lo primero que pide el programa es el largo del arreglo, este debe ser un número entero mayor a 0, si ingresa otra cosa el programa finaliza de manera automática.

Luego tenemos un menú que se repite hasta que seleccione la opción que señala el fin del programa. Las opciones son las siguientes:

**Opción 1, Ingresar un número:** Si ingresa el número 1 entonces el programa va a solicitar cual es el número que desea agregar al arreglo. Este debe de ser un número entero, sino el programa finaliza. El número lo localiza segun la función hash y la solucion en caso de colision señalada con parámetro de entrada. Retorna al menu por si desea agregar otra cosa o buscar, o salir.

**Opción 2, Buscar un número:** Si ingresa el número 2 entonces el programa solicita cual es el número que quiere buscar. Lo hace mediante la funcion hash y el metodo de solución de colisión. Retorna al menú si desea ingresar, buscar o salir.

**Opción Otro, Salir:** Si ingresa cualquier cosa que no sea 1 o 2 el programa finaliza.

La salida forzosa del programa en cualquier parte de su ejecución es mediante `ctl+z`o `ctl+c`.

## Despliegue

La aplicación se debe ejecutar desde la terminal localizada en la carpeta que presenta los archivos de ésta. Se debe de ejecutar bajo el sistema operativo Linux (señalado en Prerrequisitos).

## Construido con
- Lenguaje c++: librería iostream.

## Autor
- Noemí Gómez Rodríguez - nogomez19@alumnos.utalca.cl
