#include <iostream>

#include "hash.h"
#include "Lista.h"
#include "Funciones_Arreglo.h"

using namespace std;

// busca un número en el arreglo normalmente o mediante el metodo de colision
void buscar(Arreglo funciones_arreglo, int n, int *arreglo, string metodo_colision, Lista *lista){
  // numero a buscar
  int numero;
  cout << "¿Qué elemento desea buscar?: ";
  cin >> numero;

  // determina posicion donde debería estar por funcion hash
  int posicion = funciones_arreglo.funcion_hash(numero, n);

  // si es ese entonces lo señala o lo busca por la solucion de colision
  if(arreglo[posicion] == numero){
    cout << "El elemento " << numero << " se encuentra en la posicion " << posicion << endl;
  }else if(arreglo[posicion]!=numero && arreglo[posicion]==NULL){
    cout << "El elemento " << numero << " no está dentro del arreglo" << endl;
  }else if(arreglo[posicion]!=numero && arreglo[posicion]!=NULL){
    if(metodo_colision == "L" || metodo_colision == "l"){
      funciones_arreglo.prueba_lineal(n, arreglo, numero, posicion, true);
    }else if(metodo_colision == "C" || metodo_colision == "c"){
      funciones_arreglo.prueba_cuadratica(n, arreglo, numero, posicion, true);
    }else if(metodo_colision == "D" || metodo_colision == "d"){
      funciones_arreglo.doble_direccion(n, arreglo, numero, posicion, true);
    }else if(metodo_colision == "E" || metodo_colision == "e"){
      funciones_arreglo.encadenamiento(arreglo, n, numero, posicion, lista, true);
    }
  }
}

// ingresa el número normalmente o mediante una solucion de colisión
void ingresar(Arreglo funciones_arreglo, int n, int *arreglo, string metodo_colision, Lista *lista){
  // numero a agregar
  int numero;
  cout << "¿Qué elemento desea agregar?: ";
  cin >> numero;

  // determinar posicion por funcion hash
  int posicion = funciones_arreglo.funcion_hash(numero, n);

  // si no hay colision lo agrega normalmente
  if(arreglo[posicion]==NULL){
    arreglo[posicion] = numero;
    // si hay colision entonces lo agrega dependiendo del método
  }else{
    if(metodo_colision == "L" || metodo_colision == "l"){
      funciones_arreglo.prueba_lineal(n, arreglo, numero, posicion, false);
    }else if(metodo_colision == "C" || metodo_colision == "c"){
      funciones_arreglo.prueba_cuadratica(n, arreglo, numero, posicion, false);
    }else if(metodo_colision == "D" || metodo_colision == "d"){
      funciones_arreglo.doble_direccion(n, arreglo, numero, posicion, false);
    }else if(metodo_colision == "E" || metodo_colision == "e"){
      funciones_arreglo.encadenamiento(arreglo, n, numero, posicion, lista, false);
    }
  }
}

// llena la lista con null
void lista_vacia(int n, int *arreglo){
  for(int i=0; i<n; i++){
    arreglo[i] = NULL;
  }
}

// menú del programa
void menu(int n, int *arreglo, string metodo_colision, Lista *lista){
  // se definen las variables
  Arreglo funciones_arreglo;
  int opcion;

  bool llena = funciones_arreglo.is_full(n, arreglo);
  // si el arreglo está lleno se detiene
  if(llena==false){
    cout << "\n1. Ingresar número." << endl;
    cout << "2. Buscar número." << endl;
    cout << "Otro para salir." << endl;
    cin >> opcion; //lo que el usuario desea hacer

    switch(opcion){
      case 1:
      // ingreso de numero
      ingresar(funciones_arreglo, n, arreglo, metodo_colision, lista);
      // imprimir arreglo
      for(int i=0; i<n; i++){
        if(arreglo[i]==NULL){
          cout << "[ ]";
        }else{
          cout << "[" << arreglo[i] << "]";
        }
      }
      // de vuelta al menu
      menu(n, arreglo, metodo_colision, lista);
      break;

      case 2:
      //busca el numero y luego se devuelve al menu
      buscar(funciones_arreglo, n, arreglo, metodo_colision, lista);
      menu(n, arreglo, metodo_colision, lista);
      break;
    }
  }
}

// procedimiento para verificar si el metodo ingresado es válido
bool colision_valida(string metodo_colision){
  bool continuar;
  if(metodo_colision=="L" || metodo_colision == "l"){
    continuar = true;
  }else if(metodo_colision=="C" || metodo_colision=="c"){
    continuar = true;
  }else if(metodo_colision=="D" || metodo_colision=="d"){
    continuar = true;
  }else if(metodo_colision=="E" || metodo_colision=="e"){
    continuar = true;
  }else{
    continuar = false;
  }
  return continuar;
}

// main
int main(int argc, char *argv[])
{
  //parámetro de entrada para determinar el metodo de solución de colisión
  string metodo_colision = argv[1];

  // si el metodo coincide con los disponibles entonces el programa seguirá con su curso
  bool continuar = colision_valida(metodo_colision);
  if(continuar){
    // definicion e ingreso del tamaño del arreglo
    int n;
    cout << "Ingrese tamaño de arreglo: ";
    cin >> n;

    // si es menor o igual a cero el arreglo no existe
    if(n>0){
      int arreglo[n];
      // llena la lista con null
      lista_vacia(n, arreglo);
      // lista enlazada
      Lista *lista = new Lista();
      // menu
      menu(n, arreglo, metodo_colision, lista);
    }
  }
  return 0;
}
